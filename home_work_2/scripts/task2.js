'use strict';

var exchangeRates = 0.0288;
var RATIO_POUNDS = 2.20462;
var cargoWeight = +prompt('Введите массу ручной клади цифрами:');

while ((cargoWeight/0) != Infinity) {
    alert('Вы ввели не число или 0! Введите массу цифрами и больше 0.');
    cargoWeight = +prompt('Введите массу ручной клади цифрами:');
}

var weightInKilograms = cargoWeight / RATIO_POUNDS;
var price;
if (weightInKilograms > 0 && weightInKilograms <= 5){
    price = 3 * weightInKilograms * exchangeRates;
    console.log(price);
    alert('Сумма к оплате ' + price);
} else if(weightInKilograms > 5 && weightInKilograms <= 10){
    price = 5 * weightInKilograms * exchangeRates;
    console.log(price);
    alert('Сумма к оплате ' + price);
} else if (weightInKilograms > 10 && weightInKilograms <= 15){
    price = 10 * weightInKilograms * exchangeRates;
    console.log(price);
    alert('Сумма к оплате ' + price);
} else if (weightInKilograms > 15){
    alert('Допустимый вес ручной клади превышен.');
}
