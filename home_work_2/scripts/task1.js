'use strict';

var CIRCLE_RADIUS = 5;

var coordinateX = +prompt('Введите координату по оси X:');

while ((coordinateX/0) != Infinity && (coordinateX/0) != -Infinity && coordinateX != 0) {
    alert('Вы ввели не число! Введите координаты цифрами.');
    coordinateX = +prompt('Введите координату по оси X:');
}

var coordinateY = +prompt('Введите координату по оси Y:');

while ((coordinateY/0) != Infinity && (coordinateY/0) != -Infinity && coordinateY != 0) {
    alert('Вы ввели не число! Введите координаты цифрами.');
    coordinateY = +prompt('Введите координату по оси Y:');
}

var hypotenuse = Math.sqrt(Math.pow(coordinateX, 2) + Math.pow(coordinateY, 2));

if (hypotenuse < CIRCLE_RADIUS || (coordinateX > CIRCLE_RADIUS || coordinateX < 0) || (coordinateY > CIRCLE_RADIUS || coordinateY < 0)) {
    alert('Точка не находится в заштрихованой области.');
} else {
    alert('Точка находится в заштрихованой области.');
}
