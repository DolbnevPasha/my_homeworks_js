'use strict';

var fixedRate = 27.724;
var sum = numberAfterChecking(+prompt('Введите сумму', ''));
var newRate = numberAfterChecking(+prompt('Введите новый курс', ''));

var result = (newRate - fixedRate) * sum;

console.log(result);

function numberAfterChecking(number) {
    while ((number/0) != Infinity) {
        alert('Вы ввели не число или число 0 и меньше!');
        number = +prompt('Введите число больше 0', '');
    }
    return number;
}
