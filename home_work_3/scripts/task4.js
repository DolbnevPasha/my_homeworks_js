'use strict';

var fruitList = 'Список фруктов:';

while (true) {

    var fruit = prompt('Впишите фрукт в список', '');

    if (!fruit) break;

    fruitList += ' ' + fruit;
}

console.log(fruitList);
