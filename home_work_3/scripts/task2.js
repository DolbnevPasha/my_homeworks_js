'use strict';

var userNumber = +prompt('Введите число', '');

while ((userNumber/0) != Infinity || userNumber <= 1) {
    alert('Вы ввели не число или 1 и меньше! Введите цифру больше 1.');
    userNumber = +prompt('Введите число', '');
}

showIsPrime(isPrime(userNumber));

function isPrime(number) {
    var result = true;
    for (var i = 2; i < number; i++) {
        if (number % i == 0) {
            result = false;
            break;
        }
    }
    return result;
}

function showIsPrime(isPrime) {
    if (isPrime) {
        console.log('Число простое');
    }else {
        console.log('Число составное');
    }
}
