'use strict';

var firstNumber = numberAfterChecking(+prompt('Введите первое число', ''));
var secondNumber = numberAfterChecking(+prompt('Введите второе число', ''));
var thirdNumber = numberAfterChecking(+prompt('Введите третье число', ''));
var indicator = prompt('Введите знак "+" что бы показать наибольшее число или знак "-" что бы показать наименьшее число', '')

while (indicator !== '+' && indicator !== '-') {
    alert('Вы ввели неверный знак');
    indicator = prompt('Введите знак "+" что бы показать наибольшее число или знак "-" что бы показать наименьшее число', '');
}

if(indicator === '+'){
    if (firstNumber >= secondNumber && firstNumber >= thirdNumber) {
        console.log(firstNumber);
    } else if (secondNumber >= firstNumber && secondNumber >= thirdNumber) {
        console.log(secondNumber);
    } else {
        console.log(thirdNumber);
    }
} else {
    if (firstNumber <= secondNumber && firstNumber <= thirdNumber) {
        console.log(firstNumber);
    } else if (secondNumber <= firstNumber && secondNumber <= thirdNumber) {
        console.log(secondNumber);
    } else {
        console.log(thirdNumber);
    }
}

function numberAfterChecking(number) {
    while ((number/0) != Infinity && (number/0) != -Infinity && number != 0) {
        alert('Вы ввели не число!');
        number = +prompt('Введите число', '');
    }
    return number;
}
