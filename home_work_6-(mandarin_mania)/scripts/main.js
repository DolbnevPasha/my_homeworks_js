'use strict';

var mandarin = document.getElementById('mandarin');
var lemon = document.getElementById('lemon');
var fruit = mandarin;
var resultText = document.getElementById('resultText');
var timeText = document.getElementById('timeText');
var startGame = document.getElementById('startGame');
var easyLevel = document.getElementById('easy');
var midlLevel = document.getElementById('midl');
var hardLevel = document.getElementById('hard');
var choiceFruitMandarin = document.getElementById('choice-mandarin');
var choiceFruitLemon = document.getElementById('choice-lemon');
var listResults = document.getElementById('listResults');
var step = 1500;
var amount = 0;
var fruitTimer;
var timer;
var gameTime = 60;
var results = [0,0,0,0,0];
var isStartedGame = false;

startGame.addEventListener('click', function (event) {
    event.preventDefault();
    if(isStartedGame){
        resetGame();
        Game();
    } else{
        Game();
    }
});

easyLevel.addEventListener('click', function (event) {
    event.preventDefault();
    step = 2000;
});

midlLevel.addEventListener('click', function (event) {
    event.preventDefault();
    step = 1500;
});

hardLevel.addEventListener('click', function (event) {
    event.preventDefault();
    step = 1000;
});

choiceFruitMandarin.addEventListener('click', function (event) {
    event.preventDefault();
    fruit = mandarin;
});

choiceFruitLemon.addEventListener('click', function (event) {
    event.preventDefault();
    fruit = lemon;
});

function positionFruit() {
    mandarin.style.top = Math.round(Math.random() * 90) + '%';
    mandarin.style.left = Math.round(Math.random() * 90) + '%';
    lemon.style.top = Math.round(Math.random() * 90) + '%';
    lemon.style.left = Math.round(Math.random() * 90) + '%';
}

function changeFruit() {
    if (fruit === mandarin) {
        mandarin.style.display = 'block';
        lemon.style.display = 'none';
        fruit = lemon;
    } else {
        lemon.style.display = 'block';
        mandarin.style.display = 'none';
        fruit = mandarin;
    }
}

function Game() {
    isStartedGame = true;
    changeFruit();
    positionFruit();

    fruitTimer = setInterval(function() {
        changeFruit();
        positionFruit();
    }, step);

    mandarin.addEventListener('click', hit);
    lemon.addEventListener('click', hit);
    timerGame();
}

function resetGame() {
    clearInterval(timer);
    clearInterval(fruitTimer);
    mandarin.removeEventListener('click', hit);
    lemon.removeEventListener('click', hit);
    resultText.textContent = 'Result';
    timeText.textContent = 'Time';
    amount = 0;
}

function hit() {
    clearInterval(fruitTimer);
    changeFruit();
    positionFruit();
    ++amount;

    resultText.textContent = 'You collected ' + amount + ' fruits';
    fruitTimer = setInterval(function() {
        changeFruit();
        positionFruit();
    }, step);
}

function timerGame() {

    var startTime = new Date().getTime();

    timer = setInterval(function() {
        var currentTime = new Date().getTime();
        var difference = currentTime - startTime;
        var seconds = Math.round(difference / 1000);

        if (seconds === gameTime) {
            clearInterval(timer);
            clearInterval(fruitTimer);
            mandarin.removeEventListener('click', hit);
            lemon.removeEventListener('click', hit);
            timeText.textContent = 'Time left';
            mandarin.style.display = 'none';
            lemon.style.display = 'none';
            saveResult();
            sortResults();
            amount = 0;
            outputResult();
            isStartedGame = false;
        } else {
            timeText.textContent = gameTime - seconds;
        }
    }, 1000);

    function saveResult() {
        results[results.length - 1] = amount;
    }

    function sortResults() {
        results.sort(sortMaxToMin);

        function sortMaxToMin(a, b) {
            if(a > b) return -1;
            if(a < b) return 1;
        }
    }
    
    function outputResult() {
        listResults.innerHTML = '';
        for (var i = 0; i < results.length; i++){
            listResults.innerHTML += '<li class="li">' + (i + 1) + ': ' + results[i] + '</li>';
        }
    }
}
