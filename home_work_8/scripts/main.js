'use strict';

function scraping(part, chapter, style) {

    part = checkIsNumber(part, 'Введите часть учебника');                                    // Проверяем введеные данные на число

    chapter = checkIsNumber(chapter, 'Введите главу учебника');                              // Проверяем введеные данные на число

    while (style !== 'number' && style !== 'dash' && style !== 'current' && style !== 'table') {      // Проверяем введеный стиль вывода
        alert('Такого стиля вывода нет');
        style = prompt('Введите один из стилей: \n "number"  \n "dash" \n "current" \n "table"', '');
    }

    var listParts = document.querySelectorAll('.list');                                      // Список частей учебника

    part = existenceCheck(part, listParts, 'Такой части в учебнике нет.', 'Введите часть учебника.\n От 1 до ');   // Проверяем наличие части в учебнике и сохраняем ее

    var selectedPart = listParts[part - 1];                                                           // Сохраняем выбранную часть учебника

    var listChapters = selectedPart.querySelectorAll('.list__item');                         // Список глав выбранной части учебника

    chapter = existenceCheck(chapter, listChapters, 'Такой главы в этой части учебника нет.', 'Введите главу учебника.\n От 1 до ');  // Проверяем наличие главы в выбранной части учебника и сохраняем ее

    var selectedChapter = listChapters[chapter - 1];                                                  // Сохраняем выбранную главу учебника
    var listTopics = selectedChapter.querySelectorAll('.list-sub__link');                    // Сохраняем список с темами главы учебника

    var partName = 'Часть ' + part;                                                                   // Сохраняем название выбранной части учебника
    var chapterName = 'Глава: ' + selectedChapter.querySelector('.list__link').innerHTML;    // Сохраняем название выбранной главы учебника
    var arrayTopics = [];                                                                             // Создаем массив с темами выбранной главы учебника

    for (var i = 0; i < listTopics.length; i++ ){
        arrayTopics.push(listTopics[i].innerHTML);                                                    // Записываем все темы главы в массив
    }

    consoleOutput(chapter, style);                                                                    // Выводим результат в консоль

    function consoleOutput(chapter, style) {
        console.log(partName);
        console.log(chapterName);

        switch (style) {
            case 'number' :
                arrayTopics.forEach(function (item, i) {
                    console.log((i + 1) + ' ' + item);
                });
                break;
            case 'dash' :
                arrayTopics.forEach(function (item, i) {
                    console.log('- ' + item);
                });
                break;
            case 'current' :
                arrayTopics.forEach(function (item, i) {
                    console.log(chapter + '.' + (i + 1) + ' ' + item);
                });
                break;
            case  'table' :
                console.table(arrayTopics);
                break;
            default :
                arrayTopics.forEach(function (item, i) {
                    console.log(item);
                });
        }
    }

    function checkIsNumber(item, message) {
        while (!isNumber(item) || item <= 0){
            alert('Вы ввели не число или 0 и меньше');
            item = +prompt(message, '');
        }
        return item;

        function isNumber (n) {
            return !isNaN(parseFloat(n) && isFinite(n));
        }
    }
    
    function existenceCheck(item, listItems, message1, message2) {                                       // функция проверяет есть ли такая часть или глава в учебнике
        while (item > listItems.length || item <= 0) {                                                   // если нету то запрашивает ввести корректное число
            alert(message1);
            item = +prompt(message2 + listItems.length, '');
        }

        item = checkIsNumber(item,'Введите число');                                             // проверяем ввел ли пользователь число

        if(item > listItems.length){                                                                     // если пользователь ввел не корректное число то начинаем проверку занново (рекурсия)
            item = existenceCheck(item, listItems, message1, message2);
        }

        return item;
    }
}
