'use strict';

/**
 * For starting - create car with fuel consumption and Oli consumption:
 * var car = Car(5, 0.5);
 *
 * Car module. Created car with custom fuel consumption and Oli consumption.
 * @param fuel consumption {number}
 * @param oil consumption {number}
 * @param maximum speed {number}
 * @return {{
 *            start: start,
 *             riding: riding,
 *            checkGas: checkGas,
 *           refueling: refueling,
 *           checkOil: checkOil,
 *           reoiling: reoiling,
 *           }}
 * @constructor
 */
function Car(consumption, consumptionOli, maxSpeedAuto) {
    if (!isNumeric(consumption)) {
        showMessage('Wrong fuel consumption', 'error');
        return;
    }

    if (!isNumeric(consumptionOli)) {
        showMessage('Wrong oil consumption', 'error');
        return;
    }

    if (!isNumeric(maxSpeedAuto)) {
        showMessage('Wrong maximum speed', 'error');
        return;
    }

    let gasBalance = 100;
    let gasConsumption = consumption / 100;
    let gasResidue;
    let gasVolume = 200;
    let oilBalance = 40;
    let oilConsumption = consumptionOli / 100;
    let oilResidue;
    let oilVolume = 50;
    let criticalOilLevel = 10;
    let maxSpeed = maxSpeedAuto;
    let INCREASED_CONSUMPTION = (gasConsumption / 100) * 15;
    let MAX_CONSUMPTION = (gasConsumption / 100) * 30;
    let ignition = false;
    let ready = false;

    /**
     * Check gas amount after riding.
     * @param distance {number} - Riding distance.
     * @param speed {number} - Riding speed.
     * @return {number} - Gas amount.
     */
    function gasCheck(distance, speed) {
        if (gasBalance <= 0) {
            return 0;
        }
        let gasForRide = 0;

        if (speed <= 60){
            gasForRide = Math.round(distance * (gasConsumption + INCREASED_CONSUMPTION));
        }else if (speed > 120){
            gasForRide = Math.round(distance * (gasConsumption + MAX_CONSUMPTION));
        }else {
            gasForRide = Math.round(distance * gasConsumption);
        }

        return gasBalance - gasForRide;
    }

    /**
     * Check oil amount after riding.
     * @param distance {number} - Riding distance.
     * @return {number} - Oil amount.
     */
    function oilCheck(distance) {
        if (oilBalance <= 0) {
            return 0;
        }

        let oilForRide = Math.round(distance * oilConsumption);

        return oilBalance - oilForRide;
    }

    /**
     * Show message for a user in the console.
     * @param message {string} - Text message for user.
     * @param type {string} - Type of the message (error, warning, log). log by default.
     */
    function showMessage(message, type) {
        let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

        switch (type) {
            case 'error':
                console.error(messageText);
                break;
            case 'warning':
                console.warn(messageText);
                break;
            case 'log':
                console.log(messageText);
                break;
            default:
                console.log(messageText);
                break;
        }
    }

    /**
     * Check car for ride.
     * @param distance {number} - Ride distance.
     * @param speed {number} - Ride speed.
     */
    function checkRide(distance, speed) {
        gasResidue = gasCheck(distance, speed);
        oilResidue = oilCheck(distance);
    }

    /**
     * Check value to number.
     * @param n {void} - value.
     * @return {boolean} - Is number.
     */
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    /**
     * Public methods object.
     */
    return {
        /**
         * Start car.
         */
        start: function() {

            if (gasBalance <= 0) {
                showMessage('You don\'t have gas. Please refuel the car.', 'error');
                ready = false;
                return;
            }

            if (oilBalance <= 0) {
                showMessage('You don\'t have oil. Please fill up oil the car.', 'error');
                ready = false;
                return;
            }

            if (oilBalance <= criticalOilLevel){
                showMessage('Critical oil level. It is necessary to add oil', 'warning');
            }

            ignition = true;
            ready = true;

            showMessage('Ingition', 'log');
        },

        /**
         * Riding function.
         * @param distance {number} - Riding distance.
         * @param speed {number} - Riding speed.
         */
        riding: function(distance, speed) {
            if (!isNumeric(distance)) {
                showMessage('Wrong distance', 'error');
                return;
            }

            if (!isNumeric(speed)) {
                showMessage('Wrong speed', 'error');
                return;
            }

            if (!ignition) {
                showMessage('You need start car', 'error');
                return;
            }

            if (!ready) {
                ignition = false;
                showMessage('You need gas station', 'error');
                return;
            }

            if (speed > maxSpeed){
                showMessage('Travel speed cannot be greater than maximum speed', 'error');
                return;
            }

            if (speed <= 0){
                showMessage('Travel speed cannot be 0 or less', 'error');
                return;

            }

            checkRide(distance, speed);

            let oilDriven = Math.round(oilBalance / oilConsumption);
            let gasDriven = 0;

            if (speed <= 60){
                gasDriven = Math.round(gasBalance / (gasConsumption + INCREASED_CONSUMPTION));
            }else if (speed > 120){
                gasDriven = Math.round(gasBalance / (gasConsumption + MAX_CONSUMPTION));
            }else {
                gasDriven = Math.round(gasBalance / gasConsumption);
            }

            if (gasResidue <= 0 && oilResidue <= 0){

                if (gasDriven <= oilDriven){
                    gasIsOver(distance);
                }

                if (gasDriven >= oilDriven){
                    oilIsOver(distance);
                }

            } else if (gasResidue <= 0) {
                gasIsOver(distance);
            } else if (oilResidue <= 0){
                oilIsOver(distance);
            }

            if (gasResidue > 0 && oilResidue > 0) {
                gasBalance = gasResidue;
                oilBalance = oilResidue;
                showMessage('You arrived. Gas balance - ' + gasResidue + '. Oil balance - ' + oilResidue, 'log');

                let time = travelTime(distance, speed);
                showMessage('Travel time ' + time + ' hours', 'log');

                if (oilBalance <= criticalOilLevel){
                    showMessage('Critical oil level. It is necessary to add oil', 'warning');
                }
            }

            /**
             * Function of the way to emptying the fuel tank.
             * @param distance {number} - Riding distance.
             */
            function gasIsOver(distance) {
                let distanceLeft = Math.round(distance - gasDriven);

                gasBalance = 0;
                oilBalance = oilBalance - (Math.round(gasDriven * oilConsumption));
                let neddedGas = Math.round(distanceLeft * gasConsumption);

                let time = travelTime(gasDriven, speed);

                ignition = false;

                showMessage('Gas over. You have driven - ' + gasDriven + '. You need ' + + neddedGas + ' liters fuel. ' + distanceLeft + 'km left.', 'warning');
                showMessage('Engine stalled', 'warning');
                showMessage('Travel time ' + time + ' hours', 'log');
            }

            /**
             * Function of the way to emptying the oil tank.
             * @param distance {number} - Riding distance.
             */
            function oilIsOver(distance) {
                let distanceLeft = Math.round(distance - oilDriven);

                oilBalance = 0;
                gasBalance = gasBalance - (Math.round(oilDriven * gasConsumption));
                let neddedOil = Math.round(distanceLeft * oilConsumption);

                let time = travelTime(oilDriven, speed);

                ignition = false;

                showMessage('Oil over. You have driven - ' + oilDriven + '. You need ' + + neddedOil + ' liters oil. ' + distanceLeft + 'km left.', 'warning');
                showMessage('Engine stalled', 'warning');
                showMessage('Travel time ' + time + ' hours', 'log');
            }

            /**
             * Travel time function.
             * @param distance {number} - Riding distance.
             * @param speed {number} - Riding speed.
             * @return {number} - Travel time.
             */
            function travelTime(distance, speed) {
                let time = distance / speed;

                return time.toFixed(2);
            }
        },

        /**
         * Check gas function.
         */
        checkGas: function() {
            showMessage('Gas - ' + gasBalance, 'log');
        },

        /**
         * Refueling function.
         * @param gas
         */
        refueling: function(gas) {
            if (!isNumeric(gas)) {
                showMessage('Wrong gas amount', 'error');
                return;
            }

            if (gasVolume === gasBalance) {
                showMessage('Gasoline tank is full', 'warning');
            } else if (gasVolume < gasBalance + gas) {
                let excessFuel = Math.round(gas - (gasVolume - gasBalance));
                showMessage('Gasoline tank is full. Excess - ' + excessFuel, 'log');
                gasBalance = gasVolume;
            } else {
                gasBalance = Math.round(gasBalance + gas);
                showMessage('Gas balance - ' + gasBalance, 'log');
            }
        },

        /**
         * Check oil function.
         */
        checkOil: function() {
            showMessage('Oil - ' + oilBalance, 'log');
            if (oilBalance <= criticalOilLevel){
                showMessage('Critical oil level. It is necessary to add oil', 'warning');
            }
        },

        /**
         * Reoiling function.
         * @param oil
         */
        reoiling: function(oil) {
            if (!isNumeric(oil)) {
                showMessage('Wrong oil amount', 'error');
                return;
            }

            if (oilVolume === oilBalance) {
                showMessage('Oil tank is full', 'warning');
            } else if (oilVolume < oilBalance + oil) {
                let excessOil = Math.round(oil - (oilVolume - oilBalance));
                showMessage('Oil tank is full. Excess - ' + excessOil, 'log');
                oilBalance = oilVolume;
            } else {
                oilBalance = Math.round(oilBalance + oil);
                showMessage('Oil balance - ' + oilBalance, 'log');
            }
        },
    };
}
