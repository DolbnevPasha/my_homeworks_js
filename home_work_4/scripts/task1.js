'use strict';

var userNumber = +prompt('Введите число больше 0', '');

while(!(isNumber(userNumber) && userNumber > 0)){
    alert('Вы ввели не число или 0 и меньше! Введите цифру больше 0.');
    userNumber = +prompt('Введите число больше 0', '');
}

showEvenNumbers(userNumber);
showEvenNumbersCycle (userNumber);

function showEvenNumbers(n) {
    if (n % 2 == 0){
        console.log(n);
    }
    if(n > 2){
        showEvenNumbers(n-1);
    }
}

function showEvenNumbersCycle(n) {
    for(n; n > 1; n--){
        if (n % 2 == 0){
            console.log(n);
        }
    }
}

function isNumber (n) {
    return !isNaN(parseFloat(n) && isFinite(n));
}
