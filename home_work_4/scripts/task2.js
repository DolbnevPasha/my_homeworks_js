'use strict';

console.log(f(5, 7, test));

function f(a, b, c) {
    a = a ? a : 2;
    b = b ? b : 3;

    var result = sum(a, b);

    if(typeof f === typeof c){
        return result = c();
    }

    return result;

    function sum(a, b) {
        return a + b;
    }
}

function test() {
    return true;
}
