'use strict';

var obj = productList();
var objByCategory = sortByCategory(prompt('По какой категории отфильтровать товары?', ''))
var sum = priceByCategory(prompt('Какой категории высчитать сумму цен всех товаров?', ''))

console.log(obj);
console.log(objByCategory);
console.log(sum);

function productList() {
    var obj = {};
    for(var i = 0; i < 10; i++) {
        var index = prompt('Введите индекс от 0 до 10', '');
        if (index === '' || index === null) break;
        while(isRepeat(obj, index)){
            alert('Такой индекс уже есть!');
            index = +prompt('Введите индекс от 0 до 10', '');
        }
        while(!(isNumber(+index) && +index >= 0 && +index < 10)){
            alert('Вы ввели не число или не от 0 и до 10! Введите индекс от 0 до 10');
            index = +prompt('Введите индекс от 0 до 10', '');
        }
        obj[index] = {
            name: prompt('Введите название продукта', ''),
            price: price(+prompt('Введите цену продукта', '')),
            category: prompt('Введите категорию продукта', '')
        }
    }
    return obj;

    function isNumber (n) {
        return !isNaN(parseFloat(n) && isFinite(n));
    }

    function isRepeat(obj, index) {
        for (var key in obj){
            if(key == index){
                return true;
            }
        }
        return false;
    }

    function price(number) {
        while(!isNumber(number) || number <= 0){
            alert('Вы ввели не число или 0 и меньше!');
            number = +prompt('Введите цену продукта', '');
        }
        return number;
    }
}

function sortByCategory(category) {
    var result = {};
    for( var key in obj){
        if( obj[key].category == category){
            result[key] = obj[key];
        }
    }
    return result;
}

function priceByCategory(category) {
    var amountForDiscounts = 5;
    var discount = 0.12;
    var result = 0;

    for( var key in obj){
        if( obj[key].category == category){
            result += obj[key].price;
        }
    }
    if(result === 0){
        alert('Товаров с такой категорией нету');
    } else if(result > amountForDiscounts){
        result = result - result * discount;
        alert('Предоставлена скидка в 12%');
    }
    return result;
}
