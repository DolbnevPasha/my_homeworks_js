'use strict';

let users = {};

let createButton = document.querySelector('#createButton');      // кнопка создания пользователей
let nameField = document.querySelector('#lastName');             // поле ввода имени пользователя

createButton.addEventListener('click', function(event) {    // добавляем событие на кнопку создания пользователей
    event.preventDefault();

    if(!nameField.value.trim()){                                         // если поле с именем пользователя пустое (или только пробелы) то пользователя не создаем
        return;
    }

    let nameValue = nameField.value;                                     // записываем введеное имя в переменную
    let date = new Date;
    let hash = date.getTime();

    let resultName = 'user' + hash;                                      // создаем id пользователя

    users[resultName] = new User(nameValue, resultName);                 // создаем нового пользователя и записываем в объект 'users'
    users[resultName].createCard();                                      // создаем карточку пользователя в таблице

    nameField.value = '';                                                // очищаем поле ввода имени пользователя
});

function User(nameUser, resultName) {                                    // функция конструктор пользователя
    let _this = this;
    let name = nameUser;
    let id = resultName;
    this.tbody = null;
    this.card = null;
    this.nameFieldCard = null;
    this.renameBtn = null;
    this.removeBtn = null;

    this.createCard = function () {
        createCard();
    }

    this.setName = function (nameUser) {
        setName(nameUser);
    }

    this.getName = function () {
        return getName();
    }

    function renameUser() {
        renamedUserId = id;
    }

    function setName(nameUser) {
        name = nameUser;
    }

    function getName() {
        return name;
    }

    function getId() {
        return id;
    }

    function createCard() {
        _this.card = document.createElement('tr');

        _this.nameFieldCard = document.createElement('td');
        _this.nameFieldCard.textContent = name;

        _this.renameBtn = document.createElement('button');
        _this.renameBtn.textContent = 'RENAME';
        let renameBtnField = document.createElement('td');
        renameBtnField.appendChild(_this.renameBtn);

        _this.removeBtn = document.createElement('button');
        _this.removeBtn.textContent = 'REMOVE';
        let removeBtnField = document.createElement('td');
        removeBtnField.appendChild(_this.removeBtn);

        _this.card.appendChild(_this.nameFieldCard);
        _this.card.appendChild(renameBtnField);
        _this.card.appendChild(removeBtnField);

        _this.tbody = document.querySelector('#tbody');
        _this.tbody.appendChild(_this.card);

        _this.renameBtn.addEventListener('click', function (event) {
            event.preventDefault();
            renameUser();
            inputRename.value = getName();
            callBackModalWindow(event);
        });
        _this.removeBtn.addEventListener('click', function(event) {
            event.preventDefault();

            _this.tbody.removeChild(_this.card);
            delete users[getId()];
        });
    }
}

let inputRename = document.getElementById('rename_save_field');          // поле для переименнования пользователя
let buttonSaveField = document.getElementById('renameSave');             // кнопка переименнования пользователя
let modalWindow = document.getElementById('modal__window');              // модальное окно
let fon = document.getElementById('fon');                                // фон модального окна
let renamedUserId = null;                                                          // ID переименноваемого пользователя

buttonSaveField.addEventListener('click', callBackSaveButton);               // довавляем событие на кнопку переименнования пользователей
fon.addEventListener('click', callBackModalWindow);                          // добавляем событие на фон модального окна

function callBackModalWindow(event) {                                              // функция callback открытия/закрытия модального окна
    event.preventDefault();
    modalWindow.classList.toggle('modal__window--show');
    fon.classList.toggle('fon--show');
}

function callBackSaveButton(event) {                                               // функция callback переименнования пользователя
    event.preventDefault();

    if(!inputRename.value.trim()){                                                 // если поле переименнования пользователя пустое(или только пробелы) то выходим из функции
        callBackModalWindow(event);
        return;
    }

    users[renamedUserId].setName(inputRename.value);                                     // записываем новое имя пользователя
    users[renamedUserId].nameFieldCard.textContent = users[renamedUserId].getName();     // меняем имя в карточке пользователя
    callBackModalWindow(event);                                                          // закрываем модальное окно
}

let initButton = document.querySelector('#initButton');                         // кнопка инициализации готовых пользователей
initButton.addEventListener('click', initUsers);                                   // добавляем событие на кнопку инициализации

let arrayUsers = [];                                                                     // массив готовых пользователей
let count = 1;
for (let i = 0; i < 10; i++){
    let resultName = 'user' + count++;
    arrayUsers[i] = new User ('NoName', resultName);                                     // создаем 10 пользователей и записываем в массив готовых пользователей
}

count = 1;
function initUsers(event) {                                                              // функция callback инициализации готовых пользователей
    event.preventDefault();
    arrayUsers.forEach(function (item) {
        let resultName = 'user' + count++;
        users[resultName] = item;                                                        // записываем готовых пользователей в общий объект 'users'
        users[resultName].createCard();                                                  // добавляем карточки в таблицу
    })
    initButton.removeEventListener('click', initUsers);                            // удаляем событие с кнопки инициализации
}
