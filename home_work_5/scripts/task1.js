'use strict';

var MIN_ARRAY_SIZE = 3;
var MAX_ARRAY_SIZE = 10;
var MAX_VALUE = 100;

var stringNumbers = prompt('Введите не меньше 3 и не больше 10 чисел через запятую', '');
var arrayNumbers = stringNumbers.split(',');

while(!(isCorrespond(arrayNumbers))){
    alert('Вы ввели неверное количество чисел или число 100 и больше или не число!');
    stringNumbers = prompt('Введите не меньше 3 и не больше 10 чисел через запятую', '');
    arrayNumbers = stringNumbers.split(',');
}

arraySorting(arrayNumbers);
paritySort(arrayNumbers);

var newStringNumbers = arrayNumbers.join(',');
console.log(newStringNumbers);


function isCorrespond(arr) {

    if(arr.length < MIN_ARRAY_SIZE || arr.length > MAX_ARRAY_SIZE){
        return false;
    }

    for (var i = 0; i < arrayNumbers.length; i++) {
        if(+arrayNumbers[i] >= MAX_VALUE || !isNumber(+arrayNumbers[i])){
            return false;
        }
    }
    return true;

    function isNumber (n) {
        return !isNaN(parseFloat(n) && isFinite(n));
    }
}

function arraySorting(arr) {

    arr.sort(compareNumeric);

    function compareNumeric(a, b) {
        return a - b;
    }
}

function paritySort(arr) {

    arr.sort(compareNumeric);

    function compareNumeric(a) {
        if (a % 2 !== 0) return 1;
        if (a % 2 === 0) return -1;
    }
}

// function paritySort(arr) {
//     var count = 0;
//
//     for(var i = 0; i < arr.length; i++) {
//         if (count === arr.length) break;
//         if (arr[i] % 2 !== 0) {
//             arr.push(arr[i]);
//             arr.splice(i, 1);
//             --i;
//         }
//         ++count;
//     }
// }
