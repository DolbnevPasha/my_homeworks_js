'use strict';

var array = [3,, 6,,, 7,, undefined, 34,,,, 97,, 43];

var userIndex = prompt('Введите индекс', '');

while(!(isNumber(+userIndex) && +userIndex >= 0) || userIndex === ''){
    alert('Вы не ввели число!');
    userIndex = prompt('Введите индекс', '');
}

isCorrectIndex(array, userIndex);

function isCorrectIndex(arr, index) {
    if(index in arr){
        alert('Элемент с таким индексом есть');
        console.log(arr[index]);
        arr.push(arr[index]);
        arr.splice(index, 1);
    }else {
        alert('Элемента с таким индексом нет');
    }
}

function isNumber (n) {
    return !isNaN(parseFloat(n) && isFinite(n));
}
