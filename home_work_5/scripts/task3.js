'use strict';

var arr = [];

function productList(name, price, category) {
    arr.push({
        name: name,
        price: isPrice(+price),
        category: category
    });
    return arr;
}

function isNumber (n) {
    return !isNaN(parseFloat(n) && isFinite(n));
}

function isPrice(number) {

    while(!isNumber(number) || number <= 0){
        alert('Вы ввели не число или 0 и меньше!');
        number = +prompt('Введите цену продукта', '');
    }
    return number;
}

function sortByPrice(begin, end) {

    var result = arr.filter(function (obj) {
        return obj.price >= begin && obj.price <= end;
    });
    return result;
}

function sortByCategory(category) {

    var result = arr.filter(function (obj) {
        return obj.category === category;
    });
    return result;
}

function goodsInCategory(category) {

    var result = 0;
    arr.forEach(function (item) {
        if (item.category === category) {
            ++result;
        }
    })
    return result;
}

function removeByName(name) {

    for(var i = 0; i < arr.length; i++)
        if(arr[i].name === name) {
            arr.splice(i, 1);
            --i;
        }
}

function arraySortUp() {

    var result = arr.slice();

    result.sort(compareNumeric);

    function compareNumeric(a, b) {
        return a.price - b.price;
    }
    return result;
}

function arraySortDown() {

    var result = arr.slice();

    result.sort(compareNumeric);

    function compareNumeric(a, b) {
        return b.price - a.price;
    }
    return result;
}

function sortByChoice(sort1, sort2) {

    var result = [];
    sort1.forEach(function(item1, i1, arr1) {
        sort2.forEach(function(item2, i2, arr2){
            if(arr1[i1].price === arr2[i2].price && arr1[i1].category === arr2[i2].category){
                result.push(arr1[i1]);
            }
        });
    });
    return result;
}

function sumPrices(sort1, sort2) {

    var sum = 0;
    sort1.forEach(function(item1, i1, arr1) {
        sort2.forEach(function(item2, i2, arr2){
            if(arr1[i1].price === arr2[i2].price && arr1[i1].category === arr2[i2].category){
                sum += arr2[i2].price;
            }
        });
    });
    return sum;
}
