'use strict';

var buttonOpenModal = document.getElementById('btn__open__modal');        // Кнопка открытия модального окна
var buttonCloseModal = document.getElementById('btn__close__modal');      // Кнопка закрытия модального окна
var modalWindow = document.getElementById('modal__window');               // Модальное окно
var fon = document.getElementById('fon');                                 // Фон модального окна

buttonOpenModal.addEventListener('click', callBackModalWindow);               // добавляем событие на кнопку открытия модального окна
buttonCloseModal.addEventListener('click', callBackModalWindow);              // добавляем событие на кнопку зыкрытия модального окна
fon.addEventListener('click', callBackModalWindow);                           // добавляем событие на фон модального окна


function callBackModalWindow(event) {                                               // функция показывает/прячет модальное окно и фон
    event.preventDefault();
    modalWindow.classList.toggle('modal__window--show');                     // Если класса нет то добавляет и наоборот
    fon.classList.toggle('fon--show');                                       // Если класса нет то добавляет и наоборот
}
