'use strict';

var listTitles = document.querySelectorAll('.menu');              // Список заголовков
var activeList = null;                                                     // активный список

for (var i = 0; i < listTitles.length; i++) {                              // добавляем событие на все заголовки
    listTitles[i].addEventListener('click', showCallBack);
}

function showCallBack(event) {                                             // функция расскрывает/скрывает список пунктов заголовка
    event.preventDefault();
    var currentTitle;                                                      // текущий заголовок

    if (activeList !== this){                                              // если не активный список то скрываем расскрытые
        for (var i = 0; i < listTitles.length; i++) {
            currentTitle = listTitles[i].querySelector('.sub_menu');

            if (currentTitle.classList.contains('show')){                  // если список заголовка расскрыт то прячем его
                currentTitle.classList.remove('show');
            }
        }
    }
    var subMenu = this.querySelector('.sub_menu');                         // Ищем список пунктов выбранного заголовка
    subMenu.classList.toggle('show');                               // расскрываем его
    activeList = this;                                                     // активным списком становится выбранный список
}
